# libraries



[![Travis build status](http://img.shields.io/travis//libraries.svg?style=flat)](https://travis-ci.org//libraries)
[![Code Climate](https://codeclimate.com/github//libraries/badges/gpa.svg)](https://codeclimate.com/github//libraries)
[![Test Coverage](https://codeclimate.com/github//libraries/badges/coverage.svg)](https://codeclimate.com/github//libraries)
[![Dependency Status](https://david-dm.org//libraries.svg)](https://david-dm.org//libraries)
[![devDependency Status](https://david-dm.org//libraries/dev-status.svg)](https://david-dm.org//libraries#info=devDependencies)